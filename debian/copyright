Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dnf
Upstream-Contact: https://github.com/rpm-software-management/dnf/issues
Source: https://github.com/rpm-software-management/dnf

Files: *
Copyright: 2009, 2012-2021, Red Hat, Inc.
License: GPL-2+

Files: debian/*
Copyright: 2018-2019 Mihai Moldovan <ionic@ionic.de>
License: GPL-2+

Files: dnf/base.py
Copyright: 2012-2018, Red Hat, Inc.
 2005, 2006, Duke University
License: GPL-2+

Files: dnf/cli/cli.py
 dnf/cli/main.py
 dnf/cli/output.py
Copyright: 2012-2018, Red Hat, Inc.
 2005, 2006, Duke University
License: GPL-2+

Files: dnf/cli/commands/__init__.py
 dnf/cli/commands/history.py
Copyright: 2012-2018, Red Hat, Inc.
 2005, 2006, Duke University
License: GPL-2+

Files: dnf/cli/completion_helper.py.in
Copyright: 2016, Red Hat, Inc.
 2015, Igor Gnatenko <i.gnatenko.brain@gmail.com>
License: GPL-2+

Files: dnf/cli/term.py
Copyright: 2013, 2014, Red Hat, Inc.
License: GPL-2+

Files: dnf/exceptions.py
Copyright: 2003, 2004, Duke University
License: GPL-2+

Files: dnf/rpm/miscutils.py
Copyright: 2003, 2004, Duke University
License: GPL-2+

Files: dnf/rpm/transaction.py
Copyright: 1999-2002, Red Hat, Inc. Distributed under GPL.
License: GPL-1

Files: dnf/yum/rpmtrans.py
Copyright: 2007, Red Hat, Inc
 2005, Duke University
License: GPL-2+

Files: doc/conf.py.in
Copyright: no-info-found
License: GPL-2+

Files: etc/*
Copyright: 2014, 2015, Igor Gnatenko <i.gnatenko.brain@gmail.com>
 2013, Elad Alfassa <elad@fedoraproject.org>
License: GPL-2+

Files: scripts/*
Copyright: 2009, 2012-2021, Red Hat, Inc.
License: GPL-2+

License: GPL-1
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public
 License v1 can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License v2 can be found in `/usr/share/common-licenses/GPL-2'.
